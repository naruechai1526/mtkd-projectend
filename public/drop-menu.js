$('.img-gear').click(function () { 
    if($('.img-gear').hasClass('rotate')){
        $('.img-gear').removeClass('rotate');
        $('.img-gear').addClass('rotateBack');
    }
    else{
        $('.img-gear').removeClass('rotateBack');
        $('.img-gear').addClass('rotate');
    }
});

$('.dropdowns-menu').hide();
$('.img-gear').click(function () { 
    $('.dropdowns-menu').slideToggle();
    $('.dropdowns-menu').toggleClass('dropdowns-menu-active');
});
// ------------------------------------------- increase / decrease value -----------------------------------------

function increaseDry() {
    var value = parseFloat(document.getElementById('dry-garbage').value, 10);
    value = isNaN(value) ? 0 : value;
    value += 0.5;
    document.getElementById('dry-garbage').value = value.toFixed(1);
  } 
function decreaseDry() {
    var value = parseFloat(document.getElementById('dry-garbage').value, 10);
    value = isNaN(value) ? 0 : value;
    value -= 0.5;
    if (value < 0) {
      value = 0.0
    }
    document.getElementById('dry-garbage').value = value.toFixed(1);
  }

function increaseWet() {
    var value = parseFloat(document.getElementById('wet-garbage').value, 10);
    value = isNaN(value) ? 0 : value;
    value += 0.5;
    document.getElementById('wet-garbage').value = value.toFixed(1);
} 
function decreaseWet() {
    var value = parseFloat(document.getElementById('wet-garbage').value, 10);
    value = isNaN(value) ? 0 : value;
    value -= 0.5;
    if (value < 0) {
        value = 0.0
    }
    document.getElementById('wet-garbage').value = value.toFixed(1);
}

function increaseRecycle() {
    var value = parseFloat(document.getElementById('recycle-garbage').value, 10);
    value = isNaN(value) ? 0 : value;
    value += 0.5;
    document.getElementById('recycle-garbage').value = value.toFixed(1);
} 
function decreaseRecycle() {
    var value = parseFloat(document.getElementById('recycle-garbage').value, 10);
    value = isNaN(value) ? 0 : value;
    value -= 0.5;
    if (value < 0) {
        value = 0.0
    }
    document.getElementById('recycle-garbage').value = value.toFixed(1);
}

function increaseDanger() {
    var value = parseFloat(document.getElementById('danger-garbage').value, 10);
    value = isNaN(value) ? 0 : value;
    value += 0.5;
    document.getElementById('danger-garbage').value = value.toFixed(1);
} 
function decreaseDanger() {
    var value = parseFloat(document.getElementById('danger-garbage').value, 10);
    value = isNaN(value) ? 0 : value;
    value -= 0.5;
    if (value < 0) {
        value = 0.0
    }
    document.getElementById('danger-garbage').value = value.toFixed(1);
}

function increaseRadiation() {
    var value = parseFloat(document.getElementById('radiation-garbage').value, 10);
    value = isNaN(value) ? 0 : value;
    value += 0.5;
    document.getElementById('radiation-garbage').value = value.toFixed(1);
} 
function decreaseRadiation() {
    var value = parseFloat(document.getElementById('radiation-garbage').value, 10);
    value = isNaN(value) ? 0 : value;
    value -= 0.5;
    if (value < 0) {
        value = 0.0
    }
    document.getElementById('radiation-garbage').value = value.toFixed(1);
}

function increaseOther() {
    var value = parseFloat(document.getElementById('other-garbage').value, 10);
    value = isNaN(value) ? 0 : value;
    value += 0.5;
    document.getElementById('other-garbage').value = value.toFixed(1);
} 
function decreaseOther() {
    var value = parseFloat(document.getElementById('other-garbage').value, 10);
    value = isNaN(value) ? 0 : value;
    value -= 0.5;
    if (value < 0) {
        value = 0.0
    }
    document.getElementById('other-garbage').value = value.toFixed(1);
}
function TRASHS() {
    Swal.fire({
        icon: 'success',
        title: 'ส่งข้อมูลเรียบร้อยแล้ว',
        showConfirmButton: false,
        timer: 1000
    })
}