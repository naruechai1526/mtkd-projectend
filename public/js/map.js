
$(function() {
    $('select.styled').customSelect();
  });

  var locations = [
    ['โรงเรียนเทิงวิทยาคม',19.6849938,100.2007154],
    ['ที่ว่าการอำเภอเทิง',19.684561,100.199845],
    ['ธนพิริยะเทิง',19.6889237,100.1995553],
    ['โรงพยาบาลเทิง',19.6889365,100.1780892],
    ['',18.7816,99.0064]
];
  /* ---------- Map ---------- */
  function initialize() {
   
      const uluru = { lat: 19.6899921, lng: 100.1990426 };
      var mapOptions = {
          zoom: 14,
          center: uluru
      };
      
      var map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
    //   const marker = new google.maps.Marker({
    //     position: new google.maps.LatLng(19.6829462,100.1972724),
    //     map: map,
    //     title: 'โรงเรียนเทิงวิทยาคม'
    //     });
    var icon = {
        url: 'logo/helpmap.png', // url
        scaledSize: new google.maps.Size(20, 20), // scaled size
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(0, 0) // anchor
    };
          var marker, i, info;
for (i = 0; i < locations.length; i++) { 
marker = new google.maps.Marker({
position: new google.maps.LatLng(locations[i][1], locations[i][2]),
map: map,
icon: icon,
});
info = new google.maps.InfoWindow();
google.maps.event.addListener(marker, 'click', (function(marker, i) {
return function() {
info.setContent(locations[i][0]);
info.open(map, marker);
}

})(marker, i));

}
          
  }






  