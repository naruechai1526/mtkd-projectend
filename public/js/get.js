var get = {

    getLocation: function() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(get.showPosition);
        } else {
            alert("Geolocation is not supported by this browser.");
        }
    },

    showPosition: function(position) {
        // https: //maps.google.com/maps?q=20.149036,99.849968&width=600&height=500&hl=en&t=&z=14&ie=UTF8&iwloc=B&output=embed
        var latlon = position.coords.latitude + "," + position.coords.longitude;
        var img_url = "https://maps.google.com/maps?q=" + latlon + "&width=600&height=500&hl=en&t=&z=14&ie=UTF8&iwloc=B&output=embed";
        document.getElementById("gmap_canvas").innerHTML = "<iframe src='" + img_url + "'>" + "</iframe>";
        $('#latitude_id').val(position.coords.latitude)
        $('#longitude_id').val(position.coords.longitude)
    }
}
