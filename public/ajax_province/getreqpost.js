$( document ).ready(function() {

	// GET REQUEST
	$("#getDistrictsOption").click(function(event){
		// $("#getPost").find('input#changPost').remove().end();
		$("#getPost").find('strong').remove().end();
		event.preventDefault();
		ajaxGet();
	});

	// DO GET
	function ajaxGet(){
		var dtid = $("#getDistrictsOption").val();
		$.ajax({
			type : "GET",
			url : "/api/districts/post/"+dtid,
			success: function(result){
				// $('#getPost input').empty();
				var postList = "";
				$.each(result, function(i, post){
					$('input#changPost').val(post.zip_code);
				});
				// console.log("Success: ", result);
			},
			error : function(e) {
				$("#getPost").append("<strong>Error</strong>");
				// console.log("ERROR: ", e);
			}
		});
	}

	$("#getDistrictsOption2").click(function(event){
		// $("#getPost2").find('input#changPost2').remove().end();
		$("#getPost2").find('strong').remove().end();
		event.preventDefault();
		ajaxGet2();
	});

	// DO GET
	function ajaxGet2(){
		var dtid = $("#getDistrictsOption2").val();
		$.ajax({
			type : "GET",
			url : "/api/districts/post/"+dtid,
			success: function(result){
				// $('#getPost2 input').empty();
				var postList = "";
				$.each(result, function(i, post){
					$('input#changPost2').val(post.zip_code);
				});
				// console.log("Success: ", result);
			},
			error : function(e) {
				$("#getPost2").append("<strong>Error</strong>");
				// console.log("ERROR: ", e);
			}
		});
	}
})
