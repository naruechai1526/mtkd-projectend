const controller = {};

controller.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM user ', (err, users) => {
            if (!users) {
                return res.json(404, 'user not found')
            }
            if (err) {
                console.log(err);
            }
            return res.status(200).json({ status: 'success', data: users })
        })
    })
};

controller.save = (req, res) => {
    //if (req.session.idadmins) {
    const data = req.body;
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO users set fname_us = ? , lname_us = ? , phone_us = ? , idcard_us = ? , sex_us = ? ,address_us = ?, status_id = 1 ,village_id = ?'
            , [data.fname_us, data.lname_us, data.phone_us, data.idcard_us, data.sex_us, data.address_us, data.village_id], (err, result) => {
                if (err) {
                    console.log(err);
                } else {
                    setTimeout(() => {
                        res.redirect('/users',);
                    }, 1000)
                }
            });
    });
    //} else {
    //res.redirect('/');
    //}
};

controller.edit = (req, res) => {
    //if (req.session.idadmins) {
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM users WHERE idusers = ?;', [id], (err, users) => {
            conn.query('SELECT * FROM sex', (err, sex) => {
                conn.query('SELECT * FROM village', (err, village) => {
                    res.render('users/edit', {
                        editusers: users[0], sex: sex, village: village, session: req.session
                    });
                });
            });
        });
    });
    //} else {
    //res.redirect('/');
    //}
};

controller.update = (req, res) => {
    //if (req.session.idadmins) {
    const data = req.body;
    const { id } = req.params;
    req.session.success = true;
    req.session.topic = "แก้ไขข้อมูลการติดต่อเสร็จเรียบร้อยแล้ว";
    req.getConnection((err, conn) => {
        conn.query('UPDATE users set fname_us = ? , lname_us = ? , phone_us = ? , idcard_us = ? , sex_us = ? ,address_us = ?, village_id = ? where idusers = ?',
            [data.fname_us, data.lname_us, data.phone_us, data.idcard_us, data.sex_us, data.address_us, data.village_id, id], (err, result) => {
                if (err) {
                    console.log(err);
                }
                setTimeout(() => {
                    res.redirect('/users');
                }, 1000)
            });
    });
    //} else {
    //res.redirect('/');
    //}
}

controller.delete = (req, res) => {
    //if (req.session.idadmins) {
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('DELETE FROM users WHERE idusers = ?', [id], (err, admins) => {
            if (err) {
                res.render('delete_err');
            } else {
                req.session.success = true;
                req.session.topic = "ลบข้อมูลการติดต่อเรียบร้อยแล้ว";
                setTimeout(() => {
                    res.redirect('/users');
                }, 1000)
            }

        });
    });
    //} else {
    //res.redirect('/');
    //}
};
module.exports = controller;