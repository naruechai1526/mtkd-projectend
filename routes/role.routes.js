const express = require('express');
const router = express.Router();

const roleController = require('../controllers/role.controller')

router.get('/role', roleController.list); //เข้าหน้ารายชื่อประชาชน
router.post('/role/save', roleController.save); //เพิ่มรายชื่อประชาชน
// router.get('/users/edit:id',userlViews.edit); //เข้าหน้าแก้ไขรายชื่อประชาชน
// router.post('/users/update:id',userlViews.update); //แก้ไขรายชื่อประชาชน
// router.get('/users/delete:id',userlViews.delete);//ยืนยันการลบข้อมูลเข้าหน้ารายชื่อประชาชน

module.exports = router;