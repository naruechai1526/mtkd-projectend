const express = require('express');
const router = express.Router();

const levelStudentController = require('../controllers/levelStudent.controller')

router.get('/levelStudent', levelStudentController.list);


module.exports = router;