import { Request } from 'express'
import multer from 'multer'

const storage = multer.memoryStorage()

const fileFilter = (req: Request, file: any, cb: any) => {
	// file is max 10mb
	// if (file.size > 10 * 1024 * 1024) {
	// 	cb(new Error('File is too large'), false)
	// }
	cb(null, file)


}

export const multerUpload = multer({ storage, fileFilter })
