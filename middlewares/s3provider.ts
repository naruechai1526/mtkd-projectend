import mime from 'mime'
import path from 'path'
import { S3Client } from '../configs/awsS3'
import { CONFIG } from '../configs/environment'

class S3Provider {
	public async uploadFile(file: any): Promise<string | any> {
		const originalPath: any = `${new Date().getTime()}${path.extname(file.originalname)}`
		const ContentType = mime.getType(originalPath)

		if (!ContentType) return new Error('Content Not Found')
		if (!originalPath) return new Error('Original Path Not Found')

		return new Promise(async (resolve, reject) => {
			// Set the parameters
			const params: any = {
				Bucket: CONFIG.AWS_BUCKET_NAME,
				Key: `${CONFIG.AWS_FILE_PATH}/${file.fieldname}/${originalPath}`,
				ACL: 'public-read',
				Body: file.buffer,
				ContentType
			}
			const s3Response: any = await S3Client.upload(params).promise()
			if (s3Response.Location) {
				resolve(s3Response.Location)
			} else {
				reject(new Error('S3 upload fail'))
			}
		})
	}
}

export default new S3Provider()
