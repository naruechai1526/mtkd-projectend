import { Request } from 'express'
import { getClientIp } from 'request-ip'
import { parse } from 'useragent'

class userAgent {
	/**
	 * IpChecker
	 */
	public getIP(req: Request): string {
		return getClientIp(req).toString()
	}

	/**
	 * BrowserChecker
	 */
	public getBrowser(req: Request): string {
		return parse(req.headers['user-agent']).toString()
	}

	/**
	 * DeviceChecker
	 */
	public getDevice(req: Request): string {
		return parse(req.headers['user-agent']).device.toString()
	}

	/**
	 * OsChecker
	 */
	public getOS(req: Request): string {
		return parse(req.headers['user-agent']).os.toString()
	}
}

export default new userAgent()
