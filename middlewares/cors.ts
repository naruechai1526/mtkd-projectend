const allowlist = ['*']

// const allowlist = [
// 	// prod
// 	'https://sansai.cmhis.org',
// 	'https://fang.cmhis.org',
// 	'https://suandok.cmhis.org',
// 	'https://nakornping.cmhis.org',
// 	'https://lamphun.cmhis.org',

// 	// stg
// 	'https://stg.sansai.cmhis.org',

// 	// dev
// 	'https://dev-sansai.cmhis.org',
// 	'https://dev-fang.cmhis.org',
// 	'https://dev-suandok.cmhis.org',
// 	'https://dev-nakornping.cmhis.org',
// 	'https://dev-lamphun.cmhis.org'
// ]

export const corsOptions: any = {
	origin: function (origin: any, callback: any) {
		//! Bypass
		if (allowlist[0] === '*') return callback(null, true)
		//! Bypass
		if (allowlist.indexOf(origin) !== -1) {
			return callback(null, true)
		} else {
			return callback(null, false)
		}
	}
}
