import { NextFunction, Request, Response } from 'express'
import passport from 'passport'
// import { CONFIG } from '../configs/environment'
// import appError from './error'

export default class Middleware {
	public isAuth(req: Request, res: Response, next: NextFunction): any {
		// const secretKey = req.headers?.secret_key

		// if (secretKey) {
		// 	if (secretKey !== CONFIG.HOME_BASE_SECRET_KEY) return next(new appError(401, 'Unauthorized'))
		// 	return next()
		// }

		return passport.authenticate('jwt', { session: false })(req, res, next)
	}
}
