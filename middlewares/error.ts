import { NextFunction, Request, Response } from 'express'

// appError is a custom error class
export default class appError extends Error {
	public status: number
	public message: string

	constructor(status: number, message: string) {
		super(message)
		this.name = this.constructor.name
		this.message = message || 'Something went wrong'
		this.status = status || 500
	}
}

const messageValidator = (error: any) => {
	return error.details
		.get('body')
		.message.replace(/^"|"$/g, '')
		.replace(/\\|\//g, '')
		.replace(/\\^"|"$/g, '')
		.replace('"', '')
		.replace(/"/g, '')
		.replace('_', ' ')
}

// initializeErrorHandling
export const errorHandler = (err: appError | any, req: Request, res: Response, next: NextFunction) => {
	// default error
	if (err instanceof appError) {
		return res.status(err.status).json({ status: 'error', message: err.message })
	}

	// validation error
	if (err.message === 'Validation failed') {
		return res.status(400).json({ status: 'error', message: messageValidator(err) })
	}

	return res.status(500).json({ status: 'error', message: 'Internal Server Error' })
}

export const errorMicroService = (error: any): void => {
	// const { status, message } = error?.response?.data

	if (error?.response?.data?.status === 'error') {
		throw new Error(error?.response?.data?.message)
	}

	if (error.code) {
		throw new Error(error.code)
	}

	throw new Error(error.message)
}
