const express = require('express');
const body = require('body-parser');
const cookie = require('cookie-parser');

const session = require('express-session');
const mysql = require('mysql');
const connection = require('express-myconnection')
const app = express();
const upload = require("express-fileupload");

app.use(upload());
app.use(cookie());
app.use(express.static('public'));
app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(body.urlencoded({ extended: true }));
app.use(session({
  secret: 'Passw0rd',
  resave: true,
  saveUninitialized: true
}));
app.use(connection(mysql, {
  host: '127.0.0.1', /* server lab :172.16.20.65 */
  user: 'root', /* :taekwondo */
  password: 'Passw0rd', /* :Passw0rd! */
  port: 3306,
  database: 'mtkd_taekwondo'
}, 'single'));

const usersRoute = require('./routes/user.routes');
const roleRoute = require('./routes/role.routes');
const levelStudentRoute = require('./routes/levelStudent.routes');

app.use('/users', usersRoute);
app.use('/roles', roleRoute);
app.use('/levelStudent', levelStudentRoute);

app.listen(8081, () => {
  console.log("🚀 Server is running on port 🚀", 8081, 'http://127.0.0.1')
  console.log("🚀 Server is running on database mtkd_taekwondo 🚀")
});
